package com.example.tzp
import android.os.Bundle
import android.widget.CalendarView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.text.SimpleDateFormat
import java.util.*

class CalendarActivity : AppCompatActivity() {

    private lateinit var currentDateTextView: TextView
    private lateinit var calendarView: CalendarView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calendar)

        currentDateTextView = findViewById(R.id.current_date_textview)
        calendarView = findViewById(R.id.calendar_view)

        val currentDate = Date()
        val formatter = SimpleDateFormat("EEE, MMM d, yyyy")
        val formattedDate = formatter.format(currentDate)
        currentDateTextView.text = formattedDate
    }
}

